// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <libOOOgg/libOOOgg.h>

#include "OGMDecodeInputPin.h"
#include "OGMDecodeFilter.h"

// TODO: reference additional headers your program requires here
