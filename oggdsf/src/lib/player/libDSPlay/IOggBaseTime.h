
#pragma once



class IOggBaseTime {
public:
	
	virtual __int64 getGlobalBaseTime() = 0;

};