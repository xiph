#pragma once
#include "iDSHelper.h"
#include <string>

using namespace std;

class IDSHELPER_API DSStringer
{
public:

	static string GUID2String(const GUID* inGUID);
};
