/******************************************************************
 * CopyPolicy: GNU Public License 2 applies
 * 
 * cdda_paranoia generation III release 10.2pre
 * Copyright (C) 2008 Monty monty@xiph.org
 *
 ******************************************************************/


#define VERSIONNUM "10.2pre"
#define VERSION "cdparanoia III release " VERSIONNUM " (August 11, 2008)\n"
